import java.io.File

import actors.RootActor
import akka.actor.{ActorSystem, Props}

import scala.concurrent.Await
import scala.concurrent.duration.Duration


object Main {

  def main(args: Array[String]): Unit = {

    implicit val system: ActorSystem = ActorSystem("data-source")

    val logger = system.log

    /** Number of files for parallel processing */
    val paralellism: Int = sys.env.getOrElse("PARALELLISM", 1).toString.toInt
    logger.debug(s"PARALELLISM: $paralellism")

    /** Path to directory with files */
    val dataDir = sys.env.getOrElse("DATA_DIR", "/repartitioned_data")
    logger.debug(s"DATA_DIR: $dataDir")

    val partitions = getPartitionNames(dataDir).to[scala.collection.mutable.Queue]

    logger.debug(s"partitions: ${partitions.toArray.mkString("|")}")

    val approvedParallVal = math.min(paralellism, partitions.length)

    if (approvedParallVal == 0) {
      logger.error("approvedParallVal = 0!")
      sys.exit(1)
    }

    logger.debug(s"approvedParallVal: $approvedParallVal")

    val rootActor = system.actorOf(Props(new RootActor(partitions)), "root")

    rootActor ! RootActor.Jobs(approvedParallVal)

    Await.ready(system.whenTerminated, Duration.Inf)

  }

  def getPartitionNames(dir: String): Array[String] = {
    /** Load paths for each csv file in directory */

    val file = new File(dir)

    file.listFiles
      .filter(_.isFile)
      .filter(_.getName.endsWith(".csv"))
      .map(_.getPath)
  }

}
