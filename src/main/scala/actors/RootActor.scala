package actors


import akka.actor.{Actor, ActorRef, Props}
import akka.event.Logging


object RootActor {

  final case class Jobs(paralellism: Int)

  final case class SuccessfulLoading(partitionPath: String)

  final case class LoadingError(error: Exception, partitionPath: String)

  final case class Stopping()

}


class RootActor(partitionQueue: scala.collection.mutable.Queue[String]) extends Actor {

  private val log = Logging.getLogger(context.system, this)

  override def preStart(): Unit = log.warning("Root actor started")

  override def postStop(): Unit = log.warning("Root actor stopped")

  private var dataLoaderRefs = List[ActorRef]()

  private def handleJobs(paralellism: Int): Unit = {
    /** Schedule file processing with specified paralellism number */
    for (actorId <- Range(0, paralellism)) {
      val dataLoader = context.actorOf(Props(new DataLoaderActor(actorId)))
      dataLoaderRefs = dataLoader :: dataLoaderRefs
      dataLoader ! DataLoaderActor.PartitionLoading(partitionQueue.dequeue)
    }
  }

  private def handleStopping(): Unit = {
    context.system.stop(self)
    context.system.terminate()
  }


  private def handleSuccessfulLoading(partitionPath: String): Unit = {
    log.info(s"Partition ${partitionPath.split("/").last} was loaded")
    if (partitionQueue.nonEmpty) {
      sender() ! DataLoaderActor.PartitionLoading(partitionQueue.dequeue)
    }
    else {
      sender() ! DataLoaderActor.Stopping
      dataLoaderRefs = dataLoaderRefs.filter(!_.equals(sender()))
      if (dataLoaderRefs.isEmpty) this.self ! RootActor.Stopping
    }
  }

  private def handleLoadingError(error: Exception, partitionPath: String): Unit = {
    log.error(s"An error occurs: ${error.getClass.getCanonicalName} while loading ${partitionPath.split('/').last}")
    log.error(error.getMessage)
    sender() ! DataLoaderActor.PartitionLoading(partitionPath)
  }


  override def receive(): Receive = {
    case message: RootActor.Jobs => handleJobs(message.paralellism)
    case message: RootActor.SuccessfulLoading => handleSuccessfulLoading(message.partitionPath)
    case message: RootActor.LoadingError => handleLoadingError(message.error, message.partitionPath)
    case RootActor.Stopping => handleStopping()
  }
}
