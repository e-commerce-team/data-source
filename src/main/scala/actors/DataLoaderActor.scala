package actors

import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.Calendar

import akka.actor.Actor
import akka.dispatch.{BoundedMessageQueueSemantics, RequiresMessageQueue}
import akka.event.Logging
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.{IOResult, Materializer, SystemMaterializer}
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}
import akka.stream.javadsl.JavaFlowSupport.Source
import akka.stream.scaladsl.FileIO
import akka.util.ByteString
import com.typesafe.config.Config
import kafka.KafkaConf
import org.apache.kafka.clients.producer.{ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration


object DataLoaderActor {

  final case class PartitionLoading(partitionPath: String)

  final case class Stopping()

}


class DataLoaderActor(dataLoaderId: Int) extends Actor with RequiresMessageQueue[BoundedMessageQueueSemantics] with KafkaConf {


  private val log = Logging.getLogger(context.system, this)
  private implicit val materializer: Materializer = SystemMaterializer(context.system).materializer

  override def preStart(): Unit = log.warning(s"Dataloder$dataLoaderId started")

  override def postStop(): Unit = log.warning(s"Dataloder$dataLoaderId stopped")


  private def getSimpleDate: String = {
    val dateTime = Calendar.getInstance.getTime
    val parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    parser.format(dateTime)
  }

  private def handleStopping(): Unit = {
    context.stop(self)
  }

  private def handlePartitionLoading(partitionPath: String): Unit = {
    /** Process specified partition and send it to broker */

    val partitionName = partitionPath.split("/").last

    log.info(s"Actor works with partition $partitionName")

    val kafkaProducerSettings = loadKafkaProducerConfig(kafkaProducerConfig, kafkaConfig)
    val topic = kafkaTopic

    val partitionStream = FileIO.fromPath(Paths.get(partitionPath))

    /** convert csv to map format */
    val csvToMapFlow = partitionStream
      .via(CsvParsing.lineScanner())
      .via(CsvToMap.toMapAsStrings())

    /** add custom fields to maps */
    val customPairsFlow = csvToMapFlow
      .map(_.slice(1, 8))
      .map(_ ++ Map("timestamp" -> getSimpleDate, "source_id" -> dataLoaderId, "partition_name" -> partitionName))

    /** convert map to json format */
    val jsonFlow = customPairsFlow
      .map(scala.util.parsing.json.JSONObject)
      .map(_.toString())

    /** send to brokers */
    val kafkaProducerFlow = jsonFlow
      .map(value => new ProducerRecord[String, String](topic, value))
      .runWith(Producer.plainSink(kafkaProducerSettings)).onComplete {
          case Success(value) => sender() ! RootActor.SuccessfulLoading(partitionPath)
          case Failure(e) => log.error(s"stream completed with failure: $e")
      }
  }

  private def loadKafkaProducerConfig(kafkaProducerConfig: Config, kafkaConfig: Config): ProducerSettings[String, String] = {
    ProducerSettings(kafkaProducerConfig, new StringSerializer, new StringSerializer)
      .withProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBootstrapServers)
      .withProperty(ProducerConfig.CLIENT_DNS_LOOKUP_CONFIG, kafkaConfig.getString(ProducerConfig.CLIENT_DNS_LOOKUP_CONFIG.replace('.', '-')))
      .withProperty(ProducerConfig.RETRIES_CONFIG, kafkaConfig.getString(ProducerConfig.RETRIES_CONFIG))
  }

  override def receive: Receive = {
    case message: DataLoaderActor.PartitionLoading => handlePartitionLoading(message.partitionPath)
    case DataLoaderActor.Stopping => handleStopping()
  }
}

