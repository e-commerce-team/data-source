package kafka

import com.typesafe.config.Config

trait KafkaConf extends Conf {
  protected lazy val kafkaConfig: Config = config.getConfig("akka.kafka.connection")
  protected lazy val kafkaProducerConfig: Config = config.getConfig("akka.kafka.producer")

  protected lazy val kafkaTopic: String = scala.util.Properties.envOrElse("KAFKA_TOPIC", "commerceRecords")
  protected lazy val kafkaBootstrapServers: String = scala.util.Properties.envOrElse("KAFKA_BOOTSTRAP_SERVERS", "localhost:9092")
}