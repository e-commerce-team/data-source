package kafka

import com.typesafe.config.{Config, ConfigFactory}

trait Conf {
  protected lazy val config: Config = ConfigFactory.load()
}

