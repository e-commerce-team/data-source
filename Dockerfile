FROM ubuntu:20.04

RUN apt-get update \
        && apt-get install -y curl openjdk-8-jre\
        && rm -rf /var/lib/apt/lists/*
        
ARG SCALA_VERSION

RUN curl "https://www.scala-lang.org/files/archive/scala-$SCALA_VERSION.deb" > scala.deb \
                && dpkg --install scala.deb 

ARG JAR_PATH

COPY $JAR_PATH entrypoint.sh /

RUN mv /*.jar /app.jar 

RUN chmod +x /app.jar \
        && chmod +x /entrypoint.sh

ENV PARALELLISM=1
ENV DATA_DIR=/repartitioned_data
ENV ENVIRONMENT=dev

RUN mkdir -p $DATA_DIR

ENTRYPOINT [ "bash", "entrypoint.sh"]

