val artifactVersion = sys.env.getOrElse("ART_VERSION", "1.0")
val organizationArtifact = sys.env.getOrElse("ORG_NAME", "test-org")
val artifactScalaVersion = sys.env.getOrElse("SCALA_VERSION", "2.12.8")
val artName = sys.env.getOrElse("ART_NAME", "test-art")
val akkaVersion = sys.env.getOrElse("AKKA_VERSION", "2.6.3")

val jarName = s"$organizationArtifact.$artName-${artifactVersion}_scala-$artifactScalaVersion.jar"

version := artifactVersion
organization := organizationArtifact
scalaVersion := artifactScalaVersion


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream-kafka" % "2.0.2",
  "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "2.0.0-RC1",
  "com.typesafe" % "config" % "1.3.3",
  "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime
)


mainClass in assembly := Some("Main")

test in assembly := {}

assemblyJarName in assembly := s"$organizationArtifact.$artName-${artifactVersion}_scala-$artifactScalaVersion.jar"

assemblyMergeStrategy in assembly := {
  case PathList("com", "typesafe", xs @ _*) =>  MergeStrategy.first
  case PathList("com", "lightbend", xs @ _*) =>  MergeStrategy.first
  case PathList("ch", "qos", xs @ _*) =>  MergeStrategy.first
  case x => (assemblyMergeStrategy in assembly).value(x)
}


